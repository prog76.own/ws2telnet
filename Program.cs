﻿using Newtonsoft.Json;
using NLog;
using PrimS.Telnet;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using WebSocketSharp;

namespace ws2telnet
{
        
    class Program
    {     
        enum CtrlType
        {
            CTRL_C_EVENT = 0,
            CTRL_BREAK_EVENT = 1,
            CTRL_CLOSE_EVENT = 2,
            CTRL_LOGOFF_EVENT = 5,
            CTRL_SHUTDOWN_EVENT = 6
        }

        static void Main(string[] args)
        {
            _closeHandler += new EventHandler(CloseHandler);
            SetConsoleCtrlHandler(_closeHandler, true);
            AppDomain.CurrentDomain.AssemblyResolve += CurrentDomain_AssemblyResolve;
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

            _exception = NLog.LogManager.GetLogger("exception");
            _logger = NLog.LogManager.GetLogger("default");
            _logger.Info("Started");
            _bridge = new Bridge()
            {
                wsUrl = Properties.Settings.Default.websocketServerAddress + Properties.Settings.Default.appID,
                telnetAddress = Properties.Settings.Default.telnetServer,
                telnetPort = Properties.Settings.Default.telnetServerPort,
                timeOutPrompt = Properties.Settings.Default.timeOutPrompt,
                timeOutCommand = Properties.Settings.Default.timeOutCommand
            };
            try
            {
                _bridge.Start();
                Console.ReadLine();
            }
            catch (Exception ex)
            {
                _exception.Error(ex, "Error");
            }
        }

        private static bool CloseHandler(CtrlType sig)
        {
            _bridge.Stop();
            switch (sig)
            {
                case CtrlType.CTRL_C_EVENT:
                case CtrlType.CTRL_LOGOFF_EVENT:
                case CtrlType.CTRL_SHUTDOWN_EVENT:
                case CtrlType.CTRL_CLOSE_EVENT:
                default:
                    return false;
            }
        }
                
        static Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            if (args.Name.StartsWith("NLog"))
                return typeof(NLog.LogManager).Assembly;
            else
                return null;
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            if (!Debugger.IsAttached)
            {
                Console.BackgroundColor = ConsoleColor.Red;
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine(((Exception)e.ExceptionObject).Message);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.BackgroundColor = ConsoleColor.Black;
                Console.WriteLine("Press any key to exit");
                Console.ReadKey();

                Environment.Exit(1);
            }
        }

        static Bridge _bridge;
        static ILogger _logger, _exception;

        [DllImport("Kernel32")]
        static extern bool SetConsoleCtrlHandler(EventHandler handler, bool add);

        delegate bool EventHandler(CtrlType sig);
        static EventHandler _closeHandler;
    }
}
