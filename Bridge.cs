﻿using Newtonsoft.Json;
using NLog;
using PrimS.Telnet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebSocketSharp;

namespace ws2telnet
{
    class WsMesage
    {
        public string type { get; set; }
        public string target { get; set; }
        public string message { get; set; }
        public string serial { get; set; }
    }

    public class Bridge
    {
        public string wsUrl { get; set; }
        public string telnetAddress { get; set; }
        public ushort telnetPort { get; set; }
        public long timeOutPrompt { get; set; }
        public long timeOutCommand { get; set; }
        public bool Active { get; private set; }

        public Bridge()
        {
            _logger = NLog.LogManager.GetLogger("ws2telnet");
            _exception = NLog.LogManager.GetLogger("exception");
        }

        public void Start()
        {
            if (Active)
                throw new Exception("Already Active");
            Active = true;
            _ws = new WebSocket(wsUrl);
            _ws.Log.Output = ws_OnLog;
            _ws.OnMessage += ws_OnMessage;
            _ws.OnClose += ws_OnClose;
            _ws.OnError += ws_OnError;
            Connect();
        }

        internal void Stop()
        {
            Active = false;
            _ws.Close();
            _logger.Info("Disconnected");
        }

        internal void Connect()
        {
            try
            {
                _logger.Info("Connecting to {0}", wsUrl);
                _ws.ConnectAsync();
            }
            catch (Exception ex)
            {
                _logger.Error("Failed");
                _exception.Error(ex, "ReceiveMessage:" + ex.Message);
            }
        }

        void ws_OnLog(LogData data, string message)
        {
            _logger.Log(_logConvert[data.Level], data.Message);
        }

        void ws_OnError(object sender, ErrorEventArgs e)
        {
            if (Active && _ws.IsAlive)
                _ws.Close();
        }

        void ws_OnClose(object sender, CloseEventArgs e)
        {
            if (Active)
                Connect();
        }

        string TrimResponse(string response)
        {
            return response.Replace("\r\n", " ");
        }

        bool RunCommand(Client client, string command)
        {
            client.Write(command + "\r\n");
            _logger.Info("Command {0}", command);
            var response = client.TerminatedReadAsync(">", TimeSpan.FromMilliseconds(timeOutCommand)).Result;
            _logger.Info("Response {0}", TrimResponse(response));
            return response.Contains("SUCCESS");
        }

        void ws_OnMessage(object sender, MessageEventArgs e)
        {
            _logger.Info("Mesage received {0}", e.Data);
            Client client = null;
            try
            {
                var message = JsonConvert.DeserializeObject<WsMesage>(e.Data);
                if (message.type.Equals("goBig") && !String.IsNullOrEmpty(message.message))
                {
                    _logger.Info("Connecting to {0}:{1}", telnetAddress, telnetPort);
                    client = new Client(telnetAddress, telnetPort, new System.Threading.CancellationToken());
                    var prompt = client.TerminatedReadAsync(">", TimeSpan.FromMilliseconds(timeOutCommand)).Result;
                    _logger.Info("Connected to {0}:{1} Welcome {2}", telnetAddress, telnetPort, TrimResponse(prompt));

                    string reply = "";
                    foreach (var command in message.message.Split(','))
                        if (RunCommand(client, command))
                            reply += "success,";
                        else
                        {
                            reply += "error,";
                            break;
                        }
                    message.message = reply.TrimEnd(',');
                    _logger.Info("Reply {0}", message.message);
                    ((WebSocket)sender).Send(JsonConvert.SerializeObject(message));
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Failed");
                _exception.Error(ex, "ProcessMessage:" + ex.Message);
            }
            finally
            {
                if (client != null)
                    client.Dispose();
            }
        }

        Dictionary<WebSocketSharp.LogLevel, NLog.LogLevel> _logConvert = new Dictionary<WebSocketSharp.LogLevel, NLog.LogLevel> {
	        {WebSocketSharp.LogLevel.Debug, NLog.LogLevel.Debug},
	        {WebSocketSharp.LogLevel.Error, NLog.LogLevel.Error},
	        {WebSocketSharp.LogLevel.Fatal, NLog.LogLevel.Fatal},
	        {WebSocketSharp.LogLevel.Info, NLog.LogLevel.Info},
	        {WebSocketSharp.LogLevel.Trace, NLog.LogLevel.Trace},
	        {WebSocketSharp.LogLevel.Warn, NLog.LogLevel.Warn},
        };

        WebSocket _ws;
        ILogger _logger, _exception;
    }
}
